GOLANGCI_LINT_IMAGE := registry.gitlab.com/gitlab-org/gitlab-build-images:golangci-lint-alpine

.PHONY: lint test cover list deps-check mocks _test test-all test-integration test-integration-race test-race

lint:
	docker run -v $(PWD):/app -w /app $(GOLANGCI_LINT_IMAGE) \
	sh -c "golangci-lint run --out-format code-climate  $(if $V,-v) \
	| tee gl-code-quality-report.json \
	| jq -r '.[] | \"\(.location.path):\(.location.lines.begin) \(.description)\"'"

_test: setup
	rm -f tests.out
	go test $(if $V,-v) $(allpackages) -count 1 -timeout 30s $(ARGS) 2>&1 | tee tests.out
	${CURDIR}/bin/go-junit-report --set-exit-code < tests.out > out/junit/test-report.xml

test:
	$(MAKE) _test

test-race:
	ARGS='-race' $(MAKE) _test

# The acceptance tests cannot count for coverage
cover: setup
	@echo "NOTE: make cover does not exit 1 on failure, don't use it to check for tests success!"
	$Q rm -f out/cover/*.out out/cover/all.merged
	$Q ARGS='-coverprofile=out/cover/unit.out' $(MAKE) test
	$Q go tool cover -html out/cover/unit.out -o out/cover/coverage.html
	@echo ""
	@echo "=====> Total test coverage: <====="
	@echo ""
	$Q go tool cover -func out/cover/unit.out

list:
	@echo $(allpackages)

deps-check:
	go mod tidy
	@if git diff --color=always --exit-code -- go.mod go.sum; then \
		echo "go.mod and go.sum are ok"; \
	else \
	echo ""; \
		echo "go.mod and go.sum are modified, please commit them";\
		exit 1; \
  fi;

mocks-check: mocks
	@if git diff --color=always --exit-code -- *mock_*; then \
			echo "mocks are ok"; \
		else \
		echo ""; \
			echo "mocks are modified, please commit them";\
			exit 1; \
	fi;

# development tools
MOCKERY_VERSION ?= 2.33.2
MOCKERY ?= ${CURDIR}/bin/mockery-$(MOCKERY_VERSION)

mocks: $(MOCKERY)
	find . -type f ! -path '*vendor/*' -name 'mock_*' -delete && \
	${MOCKERY} --dir=./internal/gitlab/ --all --inpackage --case underscore

$(MOCKERY): OS_TYPE ?= $(shell uname -s)
$(MOCKERY): DOWNLOAD_URL = "https://github.com/vektra/mockery/releases/download/v$(MOCKERY_VERSION)/mockery_$(MOCKERY_VERSION)_$(OS_TYPE)_x86_64.tar.gz"
$(MOCKERY):
	# Installing $(DOWNLOAD_URL) as $(MOCKERY)
	@mkdir -p $(shell dirname $(MOCKERY))
	@curl -sL "$(DOWNLOAD_URL)" | tar xz -O mockery > $(MOCKERY)
	@chmod +x "$(MOCKERY)"

GITLAB_CHANGELOG_VERSION ?= latest
GITLAB_CHANGELOG = ${CURDIR}/bin/gitlab-changelog-$(GITLAB_CHANGELOG_VERSION)

$(GITLAB_CHANGELOG): OS_TYPE ?= $(shell uname -s | tr '[:upper:]' '[:lower:]')
$(GITLAB_CHANGELOG): DOWNLOAD_URL = "https://storage.googleapis.com/gitlab-runner-tools/gitlab-changelog/$(GITLAB_CHANGELOG_VERSION)/gitlab-changelog-$(OS_TYPE)-amd64"
$(GITLAB_CHANGELOG):
	# Installing $(DOWNLOAD_URL) as $(GITLAB_CHANGELOG)
	@mkdir -p $(shell dirname $(GITLAB_CHANGELOG))
	@curl -sL "$(DOWNLOAD_URL)" -o "$(GITLAB_CHANGELOG)"
	@chmod +x "$(GITLAB_CHANGELOG)"

generate_changelog: export CHANGELOG_RELEASE ?= $(VERSION)
generate_changelog: $(GITLAB_CHANGELOG)
	# Generating new changelog entries
	@$(GITLAB_CHANGELOG) -project-id 16573099 \
		-release $(CHANGELOG_RELEASE) \
		-starting-point-matcher "v[0-9]*.[0-9]*.[0-9]*" \
		-config-file .gitlab/changelog.yml \
		-changelog-file CHANGELOG.md

generate_certs:
	 . ./internal/testdata/certs/generate_certs.sh && gen_all
